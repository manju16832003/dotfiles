// an example to remove mapkey `Ctrl-i`
// unmap('<Ctrl-i>');

settings.omnibarSuggestionTimeout = 100;
settings.modeAfterYank = 'Normal';
settings.defaultSearchEngine = 'd';

// https://github.com/brookhong/Surfingkeys/issues/861
settings.cursorAtEndOfInput = false;

Hints.style('border: solid 3px #3c3445; color:#f5eff3; background: none; background-color: #3c3445;');
Hints.style('border: solid 3px #3c3445; color:#604c60; background: none; background-color: #f5eff3;', 'text');

Visual.style('marks', 'background-color: #9ccbdd;');
Visual.style('cursor', 'background-color: #9c6b80;');

map('<Ctrl-[>', '<Esc>');

map('u', 'e');  // right-handed scroll up
map('F', 'af'); // open in new tab
map('K', 'E');  // up tab (vertical stacking)
map('J', 'R');  // down tab

cmap('<Ctrl-j>', '<Tab>');
cmap('<Ctrl-k>', '<Shift-Tab>');

settings.theme = '\
.surfingkeys_cursor { \
    background: #9c6b80 !important; \
    color: #9c6b80 !important; \
} \
.sk_theme { \
    background: #3c3445; \
    color: #f5eff3; \
} \
.sk_theme tbody { \
    background: #3c3445; \
    color: #f5eff3; \
} \
.sk_theme input { \
    color: #f5eff3; \
} \
.sk_theme .url { \
    color: #555; \
} \
.sk_theme .annotation { \
    background: #3c3445; \
    color: #f5eff3; \
} \
.sk_theme .focused { \
    background: #3c3445; \
    color: #f5eff3; \
} \
.sk_theme .omnibar_highlight { \
    color: #9ccbdd; \
    font-weight: bold; \
} \
.sk_theme .omnibar_folder { \
    color: #6e916e; \
} \
.sk_theme .omnibar_timestamp { \
    color: #d6ade8; \
} \
.sk_theme .omnibar_visitcount { \
    color: #6698aa; \
} \
#sk_omnibarSearchResult ul li { \
    background: #3c3445 !important; \
} \
#sk_omnibarSearchResult ul li div.url { \
    color: #98b2fb; \
    font-weight: normal; \
} \
#sk_omnibarSearchResult ul li.focused { \
    background: #9c6b80 !important; \
} \
#sk_banner { \
    color: #f5eff3; \
    background: #3c3445; \
    border-color: #3c3445; \
} \
';
