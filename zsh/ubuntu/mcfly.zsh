export MCFLY_FUZZY=true
export MCFLY_KEY_SCHEME=vim

if [[ -r "/usr/share/doc/mcfly/mcfly.zsh" ]]; then
  source "/usr/share/doc/mcfly/mcfly.zsh"
fi
