[[ -n $ZPLUG_HOME && -d $ZPLUG_HOME ]] || return

source $ZPLUG_INSTALL/init.zsh

zplug "arzzen/calc.plugin.zsh"
zplug "zsh-users/zsh-syntax-highlighting"
zplug "mafredri/zsh-async"
zplug "hlissner/zsh-autopair"
zplug "softmoth/zsh-vim-mode"
zplug "lainiwa/zsh-manydots-magic" # fork of knu/zsh-manydots-magic with plugin

# Install plugins if there are plugins that have not been installed
if ! zplug check --verbose; then
    printf "Install? [y/N]: "
    if read -q; then
        echo; zplug install
    else
        echo
    fi
fi

# Then, source plugins and add commands to $PATH
zplug load
