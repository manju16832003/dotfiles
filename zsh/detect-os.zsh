if [[ `uname` == 'Linux' ]]; then
  export OS=`lsb_release -i -s | tr '[:upper:]' '[:lower:]'`

  export ZPLUG_HOME=~/.zplug

  if [[ "$OS" == 'arch' ]]; then
    export ZPLUG_INSTALL=/usr/share/zsh/scripts/zplug
  elif [[ "$OS" == 'ubuntu' ]]; then
    export ZPLUG_INSTALL=/usr/share/zplug
  fi
elif [[ `uname` == 'Darwin' ]]; then
  export OS=osx
  export ZPLUG_HOME=/usr/local/opt/zplug
  export ZPLUG_INSTALL=/usr/local/opt/zplug
fi
