runtime plugins.vim
runtime options.vim
runtime statusline.vim
runtime mappings.vim
runtime! functions/*.vim

colorscheme warm-dark

"""""""""""""""""
" custom commands

command! W w !sudo tee % > /dev/null
command! C w !xsel -i -b
command! CC %w !xsel -i -b
command! P r !xsel -o -b
command! J %!python -m json.tool
" :K      kill buffer (functions/kwbd.vim)
" :F[n]   delete n surrounding lines (functions/unfunc.vim)

"""""""""""""""
" startup tasks

let s:undos = split(globpath(&undodir, '*'), "\n")
call filter(s:undos, 'getftime(v:val) < localtime() - (60 * 60 * 24 * 90)')
call map(s:undos, 'delete(v:val)')

"""""""
" hooks

augroup java_tabs
  autocmd!
  autocmd FileType groovy setlocal tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab
  autocmd FileType java setlocal tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab
  autocmd BufNewFile,BufReadPre pom.xml setlocal tabstop=4 softtabstop=4 shiftwidth=4 expandtab
  autocmd BufReadPost Jenkinsfile set syntax=groovy
augroup end

augroup i3config_ft_detection
  autocmd!
  autocmd BufNewFile,BufReadPre ~/.config/i3/config set filetype=i3config
  autocmd BufNewFile,BufReadPre ~/.dotfiles/i3/config set filetype=i3config
augroup end

augroup misc_syntaxes
  autocmd!
  autocmd BufReadPost .eslintrc set syntax=json
  autocmd BufReadPost *.cql set filetype=sql
  autocmd BufReadPost *.sgml set filetype=xml
  autocmd BufReadPost *.sls set filetype=yaml
  autocmd BufReadPost *.pug set foldmethod=indent
augroup end

augroup lint_on_write_or_switch
  autocmd!
  autocmd BufWritePost,BufEnter * Neomake
augroup end

augroup window_changes
  autocmd!
  autocmd VimResized * wincmd =
augroup end

augroup reload_vimrc
  autocmd!
  autocmd BufWritePost *.vim,.vimrc,vimrc so $MYVIMRC
augroup end

" augroup disable_current_word_highlight
"   autocmd!
"   autocmd BufAdd *.md setlocal vim_current_word_disabled_in_this_buffer=1
" augroup end
