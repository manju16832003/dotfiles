#######
# setup

bindkey -v                    # vi mode!
autoload -U compinit colors
compinit
colors
zstyle :compinstall filename '/home/dian/.zshrc'

export KEYTIMEOUT=25 # enough time for vim mode to process

#########
# history

HISTFILE=~/.histfile
HISTSIZE=1073741824           # 2^30
SAVEHIST=$HISTSIZE

setopt HIST_IGNORE_DUPS       # forget multiple issuances of the same command
setopt HIST_SAVE_NO_DUPS
setopt HIST_REDUCE_BLANKS
setopt APPEND_HISTORY         # don't blow other sessions' histories away
setopt EXTENDED_HISTORY       # timestamps

###############
# other options

setopt NO_FLOW_CONTROL        # ctrl+s/ctrl+q shouldn't do anything
setopt RM_STAR_WAIT           # stay frosty out there
setopt TRANSIENT_RPROMPT      # don't display RPROMPT for previously accepted lines; only display it next to current line

###############
# other sources

for f in $HOME/.zsh/*.zsh; do
  source "$f"
done

# os-specific stuff
if [[ -d $HOME/.zsh/$OS ]]; then
  for f in $HOME/.zsh/$OS/*.zsh; do
    source "$f"
  done
fi

#########
# prompts

CURUSER='%(!.%F{6}.%F{15})%n%F{reset}'
STATUS='%(?.%F{14}@.%K{14}%F{1}!%k)%F{reset}'
SYS='%F{15}%m%F{reset}'
DIR='%F{9}%~%F{reset}'

PROMPT='$CURUSER$STATUS$SYS $DIR ${vim_mode} %F{reset}'
RPS1='$(git_prompt_string)'

#########
# aliases

if [ $OS = 'arch' ] || [ $OS = 'ubuntu' ]; then
  alias pacman='pikaur'
  alias ls='ls -h --color'
  alias listening='ss -plnt'
  alias i3cfg="egrep ^bind ~/.config/i3/config | cut -d ' ' -f 2- | sed 's/ /\t/' | column -ts $'\t' | pr -2 -w 145 -t | less"
else
  alias ls='ls -G'
fi

alias ll='ls -al'
alias sudo='sudo '
alias grep='grep --color'
alias less='less -Xr'
alias locate='locate -i'
alias rm='rm -v'
alias vi='nvim'
alias vim='nvim'
alias tree='tree -C'
alias trii='tree -C -I $(git check-ignore * 2>/dev/null | tr "\n" "|").git'
alias vpn='cd /etc/openvpn && sudo openvpn --config /etc/openvpn/vpn.conf'
alias rot13='tr a-zA-Z n-za-mN-ZA-M'

# the fuck
type fuck >/dev/null 2>&1 && { eval $(thefuck --alias) }

# pop a calendar
type when >/dev/null 2>&1 && { when ci }
