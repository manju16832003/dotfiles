# hi

These are my dotfiles. Everything in here can be safely assumed to be in flux to a greater or lesser extent. The important parts:

* Arch/Ubuntu/OSX
* i3wm
* zsh
* neovim

## installation

Execute `install`. This automatically detects your OS, but the `-c` flag allows you to specify an alternative config file by full name (e.g. `ubuntu.conf.yaml`).
