"hi clear
syntax reset
let g:colors_name = 'warm-dark'
let g:terminal_color_0='#604c60'
let g:terminal_color_8='#604c60'
" red
let g:terminal_color_1='#9c6b80'
let g:terminal_color_9='#dc98ac'
" green
let g:terminal_color_2='#6e916e'
let g:terminal_color_10='#96c497'
" yellow
let g:terminal_color_3='#9c8969'
let g:terminal_color_11='#d9bd8f'
" blue
let g:terminal_color_4='#6582c7'
let g:terminal_color_12='#98b2fb'
" magenta
let g:terminal_color_5='#a276b6'
let g:terminal_color_13='#d6ade8'
" cyan
let g:terminal_color_6='#6698aa'
let g:terminal_color_14='#9ccbdd'
" white
let g:terminal_color_7='#cfc7cd'
let g:terminal_color_15='#f5eff3'

hi Boolean gui=NONE guifg=#d6ade8 guibg=NONE
hi ColorColumn gui=NONE guifg=NONE guibg=#3c3445
hi Comment gui=italic guifg=#dc98ac guibg=NONE
hi Conceal gui=NONE guifg=#9c6b80 guibg=NONE
hi Conditional gui=NONE guifg=#d6ade8 guibg=NONE
hi Constant gui=NONE guifg=#d9bd8f guibg=NONE
hi Cursor gui=reverse guifg=NONE guibg=NONE
hi CursorColumn gui=NONE guifg=NONE guibg=NONE
hi CursorLine gui=NONE guifg=NONE guibg=#604c60
hi CursorLineNr gui=NONE guifg=NONE guibg=#604c60
hi DiffAdd gui=standout guifg=#6e916e guibg=NONE
hi DiffChange gui=bold guifg=NONE guibg=NONE
hi DiffDelete gui=standout guifg=#9c6b80 guibg=NONE
hi DiffText gui=standout guifg=NONE guibg=NONE
hi Directory gui=bold guifg=#f5eff3 guibg=NONE
hi Error gui=bold,underline guifg=#dc98ac guibg=NONE
hi ErrorMsg gui=bold guifg=#9c6b80 guibg=#9ccbdd
hi FoldColumn gui=NONE guifg=#9c6b80 guibg=NONE
hi Folded gui=NONE guifg=#dc98ac guibg=NONE
hi Ignore gui=NONE guifg=NONE guibg=NONE
hi IncSearch gui=NONE guifg=#3c3445 guibg=#9c6b80
hi LineNr gui=NONE guifg=NONE guibg=NONE
hi MatchParen gui=NONE guifg=#3c3445 guibg=#9ccbdd
hi ModeMsg gui=NONE guifg=NONE guibg=NONE
hi MoreMsg gui=NONE guifg=NONE guibg=NONE
hi NonText gui=NONE guifg=#3c3445 guibg=NONE
hi Normal gui=NONE guifg=#f5eff3 guibg=NONE
hi Number gui=NONE guifg=#d6ade8 guibg=NONE
hi Pmenu gui=NONE guifg=NONE guibg=#3c3445
hi PmenuSbar gui=NONE guifg=NONE guibg=#9c6b80
hi PmenuSel gui=NONE guifg=NONE guibg=#9c6b80
hi PmenuThumb gui=NONE guifg=NONE guibg=#dc98ac
hi Question gui=NONE guifg=NONE guibg=NONE
hi Search gui=italic guifg=#3c3445 guibg=#9ccbdd
hi SignColumn gui=NONE guifg=#d9bd8f guibg=NONE
hi Special gui=NONE guifg=#d6ade8 guibg=NONE
hi SpecialKey gui=NONE guifg=#616161 guibg=NONE
hi SpellBad gui=undercurl guisp=NONE guifg=NONE guibg=NONE
hi SpellCap gui=undercurl guisp=NONE guifg=NONE guibg=NONE
hi SpellLocal gui=undercurl guisp=NONE guifg=NONE guibg=NONE
hi SpellRare gui=undercurl guisp=NONE guifg=NONE guibg=NONE
hi Statement gui=NONE guifg=#9ccbdd guibg=NONE
hi StatusLine gui=NONE guifg=NONE guibg=NONE
hi StatusLineNC gui=italic guifg=#dc98ac guibg=NONE
hi StorageClass gui=NONE guifg=#d9bd8f guibg=NONE
hi String gui=NONE guifg=#96c497 guibg=NONE
hi TabLine gui=NONE guifg=#3c3445 guibg=#dc98ac
hi TabLineFill gui=NONE guifg=NONE guibg=#604c60
hi TabLineSel gui=reverse guifg=#3c3445 guibg=#dc98ac
hi Title gui=NONE guifg=NONE guibg=NONE
hi Todo gui=bold guifg=#9c6b80 guibg=#9ccbdd
hi Type gui=NONE guifg=#d6ade8 guibg=NONE
hi Underlined gui=NONE guifg=NONE guibg=NONE
hi VertSplit gui=NONE guifg=NONE guibg=NONE
hi Visual gui=NONE guifg=NONE guibg=#604c60
hi VisualNOS gui=NONE guifg=NONE guibg=NONE
hi WarningMsg gui=NONE guifg=#9c6b80 guibg=#9ccbdd
hi WildMenu gui=NONE guifg=NONE guibg=NONE
hi lCursor gui=NONE guifg=NONE guibg=NONE
hi Identifier gui=NONE guifg=NONE guibg=NONE
hi PreProc gui=NONE guifg=NONE guibg=NONE
hi GitGutterAdd gui=NONE guifg=#96c497 guibg=NONE
hi GitGutterChange gui=NONE guifg=#d9bd8f guibg=NONE
hi GitGutterChangeDelete gui=NONE guifg=#d9bd8f guibg=NONE
hi GitGutterDelete gui=NONE guifg=#dc98ac guibg=NONE
hi NeomakeVirtualTextWarningDefault	gui=NONE guifg=#9ccbdd guibg=None
hi NeomakeVirtualTextErrorDefault	gui=NONE guifg=#9ccbdd guibg=None
hi SignatureMarkText gui=NONE guifg=#9ccbdd guibg=None
