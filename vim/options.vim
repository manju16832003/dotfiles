scriptencoding 'utf-8'

"""""""""""""""""
" syntax settings

syntax enable
let groovy_fold='true'

""""""""""""""""""
" system/workspace

set autoread                    " reload changed files from disk
set background=dark
set backspace=indent,eol,start  " delete indents and new lines
set clipboard=unnamedplus
set cursorline
set hidden                      " track hidden buffers
set lazyredraw                  " update after finishing macro execution etc
set mousefocus
set mouse=a
set nojoinspaces                " only one space with join
set number                      " line number for cursor row only
set relativenumber              " other lines numbered in movements
set scrolloff=3                 " start scrolling 3 lines before border
set scrollopt=ver,hor,jump      " tie scrollbound splits together
set showcmd                     " show commands in progress
set title
set titlestring=%t%(\ %M%)%(\ (%{expand(\"%:~:.:h\")})%)%(\ %a%)
set ttimeoutlen=100             " for alt-hjkl buffer switching: https://github.com/neovim/neovim/issues/5792
set undofile
set undodir=~/.vim/undo         " make undo persistent across sessions
set updatetime=250

if has('nvim')
  let g:terminal_scrollback_buffer_size = 10000
  set termguicolors             " use gui instead of cterm highlights in neovim terminal
endif

"""""""""""""""""""
" character display

set linebreak                   " better line breaks
set showbreak=❧                 " mark line breaks
set list
set listchars=tab:▸\ ,trail:·,eol:¶             " mark end of line

""""""""""""""
" tab defaults

set expandtab
set shiftwidth=2
set smarttab
set softtabstop=2
set tabstop=2

"""""""""""""""""""""""""""""
" wildmenu command completion

set wildmenu
set wildmode=longest,list
set wildignorecase
set wildignore+=*.a,*.o,*.pyc,*.class,*.so
set wildignore+=*.bmp,*.gif,*.ico,*.jpg,*.png
set wildignore+=.DS_Store,.git,.hg,.svn
set wildignore+=*~,*.swp,*.tmp

"""""""""
" folding

set foldcolumn=1
set foldmethod=syntax
set foldlevelstart=20

"""""""""""
" searching

set ignorecase
set smartcase
set wrapscan
set inccommand=nosplit          " neovim: realtime ex command view
