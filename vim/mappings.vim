""""""""""
" mappings

" undo
nnoremap <F4> :MundoToggle<CR>

" and in insert mode!
inoremap <C-z> <C-g>u<C-w>
inoremap <C-u> <C-g>u<C-u>

" disable ex-mode
nnoremap Q q

" up/down in wrapped lines
nnoremap j gj
nnoremap k gk
vnoremap j gj
vnoremap k gk

" screen up/down
nnoremap <C-j> <C-e>
nnoremap <C-k> <C-y>

" hide search highlighting
map <Leader>\ :noh<CR>

" duplicate a selection in visual mode
vmap D y'>p

" select all
nnoremap <C-a> ggvG$

" insert a single character
nnoremap <leader>I I <Esc>r
nnoremap <leader>i i <Esc>r
nnoremap <leader>A A <Esc>r
nnoremap <leader>a a <Esc>r

" next/previous lint symbols
nnoremap <space>n :lnext<CR>
nnoremap <space>p :lprev<CR>

""""""""""""""
" command line

cnoremap <C-j> <Down>
cnoremap <C-k> <Up>
cnoremap <C-h> <Left>
cnoremap <C-l> <Right>
cnoremap <C-b> <C-Left>
cnoremap <C-f> <C-Right>

" cmdline paste
cnoremap <C-p> <C-r>*

""""""""
" denite

nnoremap <C-p> :Denite -direction=topleft -start-filter file/rec<CR>
nnoremap <space>/ :Denite -direction=topleft -start-filter grep<CR>
nnoremap <space>s :Denite -direction=topleft -start-filter buffer<CR>
nnoremap <space>m :Denite -direction=topleft -start-filter mark<CR>
nnoremap <space>t :Denite -direction=topleft -start-filter outline<CR>
nnoremap <space>c :Denite -direction=topleft -start-filter command_history<CR>
nnoremap <space>h :Denite -direction=topleft -start-filter help<CR>
nnoremap <space>r :Denite -direction=topleft -start-filter register<CR>

function! s:denite_my_settings() abort
  nnoremap <silent><buffer><expr> <CR> denite#do_map('do_action')
  nnoremap <silent><buffer><expr> p denite#do_map('do_action', 'preview')
  nnoremap <silent><buffer><expr> q denite#do_map('quit')
  nnoremap <silent><buffer><expr> <C-[> denite#do_map('quit')
  nnoremap <silent><buffer><expr> <Space> denite#do_map('toggle_select').'j'
endfunction

if has('nvim')
  function! s:denite_filter_my_settings() abort
    inoremap <silent><buffer><expr> <CR> denite#do_map('do_action')

    " quit _everything_ instead of just closing filter window with denite_filter_quit
    imap <silent><buffer><expr> <C-[> denite#do_map('quit')
    imap <silent><buffer><expr> <Esc> denite#do_map('quit')

    " C-j/C-k to navigate denite buffer from filter, faster neovim version
    inoremap <silent><buffer><expr> <C-j> denite#increment_parent_cursor(1)
    inoremap <silent><buffer><expr> <C-k> denite#increment_parent_cursor(-1)
    nnoremap <silent><buffer><expr> <C-j> denite#increment_parent_cursor(1)
    nnoremap <silent><buffer><expr> <C-k> denite#increment_parent_cursor(-1)
  endfunction
else
  function! s:denite_filter_my_settings() abort
    inoremap <silent><buffer><expr> <CR> denite#do_map('do_action')

    " quit _everything_ instead of just closing filter window with denite_filter_quit
    imap <silent><buffer><expr> <C-[> denite#do_map('quit')
    imap <silent><buffer><expr> <Esc> denite#do_map('quit')

    " C-j/C-k to navigate denite buffer from filter
    inoremap <silent><buffer> <C-j> <Esc><C-w>p:call cursor(line('.')+1,0)<CR><C-w>pA
    inoremap <silent><buffer> <C-k> <Esc><C-w>p:call cursor(line('.')-1,0)<CR><C-w>pA
  endfunction
endif

augroup denite_setup
  autocmd FileType denite call s:denite_my_settings()
  autocmd FileType denite-filter call s:denite_filter_my_settings()
augroup end

""""""""
" splits

" new, absolute positions
nmap <leader>swh :topleft  vnew<CR>
nmap <leader>swl :botright vnew<CR>
nmap <leader>swk :topleft  new<CR>
nmap <leader>swj :botright new<CR>

" new, relative positions
nmap <leader>sh  :leftabove  vnew<CR>
nmap <leader>sl  :rightbelow vnew<CR>
nmap <leader>sk  :leftabove  new<CR>
nmap <leader>sj  :rightbelow new<CR>

" navigate splits
imap <C-w> <C-o><C-w>
nnoremap <A-h> <C-W>h
nnoremap <A-j> <C-W>j
nnoremap <A-k> <C-W>k
nnoremap <A-l> <C-W>l

""""""""""
" terminal

if has('nvim')
  tnoremap <C-[><C-[> <C-\><C-n>
endif

"""""""""
" for osx

nnoremap ˙ <C-W>h
nnoremap ∆ <C-W>j
nnoremap ˚ <C-W>k
nnoremap ¬ <C-W>l

"""""""""""""""""""""""""""""""""""""""""""
" add undo points at punctuation, for prose
"
" :h i_CTRL-G_u
" https://twitter.com/vimgifs/status/913390282242232320
inoremap . .<C-g>u
inoremap ? ?<C-g>u
inoremap ! !<C-g>u
inoremap , ,<C-g>u
