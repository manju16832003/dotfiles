"""""""""""""""""""
" plugin management

set nocompatible
filetype off

set runtimepath^=~/.vim/dein.vim

call dein#begin(expand('~/.vim/plugins'))

call dein#add('Shougo/dein.vim')                " plugin management

" integrations, major modules
call dein#add('Shougo/denite.nvim')             " find/navigate files
call dein#add('simnalamburt/vim-mundo')         " better undo
call dein#add('Shougo/vimproc.vim', {'build': 'make'})  " async exec
call dein#add('editorconfig/editorconfig-vim')  " respect .editorconfig
call dein#add('neomake/neomake')                " run static analysis checks
call dein#add('ludovicchabant/vim-gutentags')   " automatic tag file management
call dein#add('simeji/winresizer')              " modal resize with C-e

" editor interface tweaks
call dein#add('airblade/vim-gitgutter')         " git line symbols
call dein#add('Yggdroot/indentLine')            " display indentation levels
call dein#add('rrethy/vim-hexokinase', { 'build': 'make hexokinase' }) " colors
call dein#add('zirrostig/vim-schlepp')          " move visual selections around
call dein#add('kshenoy/vim-signature')          " marks in the sign column
call dein#add('wellle/context.vim')             " show indent-based context

" editing improvements
call dein#add('zirrostig/vim-schlepp')          " move visual selections around
call dein#add('Raimondi/delimitMate')           " auto-close symbol pairs
call dein#add('terryma/vim-multiple-cursors')   " multicursor mode
call dein#add('tommcdo/vim-exchange')           " exchange text objects
call dein#add('tpope/vim-commentary')           " comment toggles
call dein#add('tpope/vim-fugitive')             " git commands
call dein#add('tpope/vim-repeat')               " repeat plugin-map commands
call dein#add('tpope/vim-surround')             " work with brackets/tags/etc
call dein#add('tpope/vim-unimpaired')           " miscellaneous improvements
call dein#add('tpope/vim-speeddating')          " C-A/C-X increment/decrement dates
call dein#add('tpope/vim-obsession')            " record session files with :Obsess
call dein#add('machakann/vim-swap')             " reorder items
call dein#add('dominikduda/vim_current_word')   " highlight word under cursor
call dein#add('andymass/vim-matchup')           " better matching with %

" syntax
call dein#add('sheerun/vim-polyglot')           " base syntax package
call dein#add('othree/xml.vim')                 " override xml plugin
call dein#add('tmhedberg/SimpylFold')           " python code folding

" lots of markdown-specific additions
call dein#add('godlygeek/tabular')              " tables
call dein#add('joker1007/vim-markdown-quote-syntax')  " highlight code
call dein#add('plasticboy/vim-markdown')        " override markdown plugin
call dein#add('mzlogin/vim-markdown-toc')       " table of contents

" textobjs
call dein#add('kana/vim-textobj-user')          " base
call dein#add('jceb/vim-textobj-uri')           " u
call dein#add('lucapette/vim-textobj-underscore') " _
call dein#add('Chun-Yang/vim-textobj-chunk')    " c
call dein#add('reedes/vim-textobj-sentence')    " s and parens, improved
call dein#add('tpope/vim-jdaddy')               " j
call dein#add('wellle/targets.vim')             " seeking, separators, a, b, q

call dein#end()

filetype plugin indent on

if dein#check_install()
  call dein#install()
endif

let g:loaded_matchit = 1

""""""""
" python

if !empty(glob('/usr/bin/python2'))
  let g:python_host_prog = '/usr/bin/python2'
elseif !empty(glob('/usr/local/bin/python'))
  let g:python3_host_prog = '/usr/local/bin/python'
elseif !empty(glob('~/work/anaconda3/envs/tools/bin/python'))
  let g:python3_host_prog = '~/work/anaconda3/envs/tools/bin/python'
endif

""""""""
" denite

if executable('rg')
  call denite#custom#var('file/rec', 'command', ['rg', '--files', '--glob', '!.git'])
  call denite#custom#var('grep', 'command', ['rg'])
  call denite#custom#var('grep', 'recursive_opts', [])
  call denite#custom#var('grep', 'final_opts', [])
  call denite#custom#var('grep', 'separator', ['--'])
  call denite#custom#var('grep', 'default_opts', ['--vimgrep', '--no-heading', '--ignore-case'])
else
  call denite#custom#var('file/rec', 'command',
      \ ['grep', '--follow', '--nocolor', '--nogroup', '-g', ''])
  call denite#custom#var('grep', 'command', ['grep'])
endif

" allow grep source filtering on either path or text
call denite#custom#source('grep', 'converters', ['converter_abbr_word'])

call denite#custom#option('default', 'prompt', '>')
call denite#custom#option('default', 'cursor_wrap', v:true)

"""""""""
" neomake

let g:neomake_warning_sign = {'text': '?', 'texthl': 'WarningMsg'}
let g:neomake_error_sign = {'text': '!', 'texthl': 'ErrorMsg'}

if executable('eslint')
  let g:neomake_javascript_enabled_makers = ['eslint']
endif

if executable('sqlint')
  let g:neomake_sql_enabled_makers = ['sqlint']
endif

if executable('shellcheck')
  let g:neomake_sh_enabled_makers = ['shellcheck']
endif

if executable('pylint')
  let g:neomake_python_enabled_makers = ['pylint']
endif

"""""""""""
" gitgutter

set signcolumn=yes
let g:gitgutter_max_signs = 500
let g:gitgutter_realtime = 1
let g:gitgutter_sign_removed = 'x'

"""""""""""""
" delimitMate

let g:delimitMate_expand_cr = 1

""""""""""""
" indentLine

let g:indentLine_setConceal = 0
let g:indentLine_setColors = 0
let g:indentLine_char = '¦'

""""""""""
" polyglot

let g:javascript_plugin_jsdoc = 1         " vim-javascript: enable jsdoc
let g:sql_type_default = 'pgsql'          " pgsql: always assume postgres
let g:vim_jsx_pretty_colorful_config = 1  " vim-jsx-pretty: full color

""""""""""""""""""
" vim_current_word

let g:vim_current_word#highlight_current_word=0

""""""""""""
" hexokinase

let g:Hexokinase_highlighters= ['backgroundfull']

"""""""""
" schlepp

vmap <C-j> <Plug>SchleppDown
vmap <C-k> <Plug>SchleppUp
vmap <C-h> <Plug>SchleppLeft
vmap <C-l> <Plug>SchleppRight

"""""""""""
" gutentags

if executable('rg')
  let g:gutentags_file_list_command = 'rg --files'
endif

"""""""""""""
" context.vim

let g:context_nvim_no_redraw = 1 " https://github.com/wellle/context.vim/issues/68
let g:context_border_char = '—'
