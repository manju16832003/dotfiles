""""""""""""""""""
" system/workspace

set autoread                    " reload changed files from disk
set background=dark
set backspace=indent,eol,start  " delete indents and new lines
set clipboard=unnamedplus
set hidden                      " track hidden buffers
set lazyredraw                  " update after finishing macro execution etc
set mousefocus
set mouse=a
set nojoinspaces                " only one space with join
set number                      " line number for cursor row only
set relativenumber              " other lines numbered in movements
set scrolloff=3                 " start scrolling 3 lines before border
set scrollopt=ver,hor,jump      " tie scrollbound splits together
set showcmd                     " show commands in progress
set updatetime=250

"""""""""""""""""""
" character display

set linebreak                   " better line breaks
set showbreak=❧                 " mark line breaks
set list
set listchars=tab:▸\ ,trail:·,eol:¶             " mark end of line

"""""""""
" folding

set foldcolumn=1
set foldmethod=syntax
set foldlevelstart=20

"""""""""""
" searching

set ignorecase
set smartcase
set wrapscan
set inccommand=nosplit          " neovim: realtime ex command view

""""""""""
" mappings

" up/down in wrapped lines
nnoremap j gj
nnoremap k gk
vnoremap j gj
vnoremap k gk

" screen up/down
nnoremap <C-j> <C-e>
nnoremap <C-k> <C-y>

" hide search highlighting
map <Leader>\ :noh<CR>

" duplicate a selection in visual mode
vmap D y'>p

" select all
nnoremap <C-a> ggvG$

" insert a single character
nnoremap <leader>I I <Esc>r
nnoremap <leader>i i <Esc>r
nnoremap <leader>A A <Esc>r
nnoremap <leader>a a <Esc>r

" next/previous lint symbols
nnoremap <space>n :lnext<CR>
nnoremap <space>p :lprev<CR>

""""""""
" splits

" new, absolute positions
nmap <leader>swh :topleft  vnew<CR>
nmap <leader>swl :botright vnew<CR>
nmap <leader>swk :topleft  new<CR>
nmap <leader>swj :botright new<CR>

" new, relative positions
nmap <leader>sh  :leftabove  vnew<CR>
nmap <leader>sl  :rightbelow vnew<CR>
nmap <leader>sk  :leftabove  new<CR>
nmap <leader>sj  :rightbelow new<CR>

" navigate splits
imap <C-w> <C-o><C-w>
nnoremap <A-h> <C-W>h
nnoremap <A-j> <C-W>j
nnoremap <A-k> <C-W>k
nnoremap <A-l> <C-W>l

"""""""""""""""""""""""""""""""""""""""""""
" add undo points at punctuation, for prose
"
" :h i_CTRL-G_u
" https://twitter.com/vimgifs/status/913390282242232320
inoremap . .<C-g>u
inoremap ? ?<C-g>u
inoremap ! !<C-g>u
inoremap , ,<C-g>u
