export VISUAL=nvim
export EDITOR=nvim
export PAGER="less -Xr"
export WINEDLLOVERRIDES="winemenubuilder.exe=d"
export GTK_THEME=Arc-Dark
export PG_COLOR="auto"
